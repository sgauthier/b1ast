BLOG_TITLE	:= Sylvain Gauthier
BLOG_DESC	:= Blog de Sylvain Gauthier, projets perso, expériences au Japon, etc.

ENTRIES_DIR := /home/sylvain/blog
DATA_DIR	:= /home/sylvain/blog/data
SCRIPT_DIR	:= ./src
HTML_DIR	:= ./html
STYLE_DIR	:= ./styles
WWW_DIR		:= /var/www/localhost/htdocs

BASE_URL	:= http://localhost
ROOT_URL	:= 


ENTRIES := $(shell find $(ENTRIES_DIR) -maxdepth 1 -type f -name *.md)
DATA := $(shell find $(DATA_DIR) -maxdepth 1 -type f)
BLOG_DIR := $(addprefix $(WWW_DIR)/,blog)
BLOG_DATA_DIR := $(addprefix $(BLOG_DIR)/,data)
POSTS_DIR := $(addprefix $(BLOG_DIR)/,posts)
BLOG_ARTICLES := $(addprefix $(POSTS_DIR)/,$(notdir $(ENTRIES:.md=.html)))
BLOG_DATA := $(addprefix $(BLOG_DATA_DIR)/,$(notdir $(DATA)))
BLOG_INDEX := $(addprefix $(BLOG_DIR)/,index.html)
BLOG_URL := $(BASE_URL)$(ROOT_URL)/blog
BLOG_RSS := $(addprefix $(BLOG_DIR)/,rss.xml)

STYLES := $(shell find $(STYLE_DIR) -maxdepth 1 -type f -name *.css)
EXPORTED_STYLES := $(addprefix $(BLOG_DIR)/,$(notdir $(STYLES)))

.PHONY: all

all: $(BLOG_DIR) $(POSTS_DIR) $(BLOG_INDEX) $(BLOG_RSS) $(BLOG_ARTICLES) $(EXPORTED_STYLES) $(BLOG_DATA_DIR) $(BLOG_DATA)

$(BLOG_DIR):
	mkdir -p $@

$(POSTS_DIR):
	mkdir -p $@

$(BLOG_DATA_DIR):
	mkdir -p $@

$(BLOG_INDEX): $(BLOG_ARTICLES) $(HTML_DIR)/header.html $(HTML_DIR)/footer.html $(SCRIPT_DIR)/make_index.sh
	ROOT_URL="$(ROOT_URL)" $(SCRIPT_DIR)/make_index.sh $(ENTRIES) > $(BLOG_INDEX)

$(BLOG_RSS): $(BLOG_ARTICLES) $(SCRIPT_DIR)/make_rss.sh
	BLOG_URL="$(BLOG_URL)" BLOG_TITLE="$(BLOG_TITLE)" BLOG_DESC="$(BLOG_DESC)" $(SCRIPT_DIR)/make_rss.sh $(ENTRIES) > $(BLOG_RSS)

$(POSTS_DIR)/%.html: $(ENTRIES_DIR)/%.md $(HTML_DIR)/header.html $(HTML_DIR)/footer.html $(SCRIPT_DIR)/make_post.sh
	ROOT_URL="$(ROOT_URL)" $(SCRIPT_DIR)/make_post.sh $< > $@

$(BLOG_DATA_DIR)/%: $(DATA_DIR)/%
	cp $^ $@

clean:
	rm -rf $(BLOG_DIR)

$(BLOG_DIR)/%.css: $(STYLE_DIR)/%.css
	cp $^ $@
