#!/bin/bash

source "src/utils.sh"

function generate_link() {
    file="$(basename "$1")"
    title="$(get_post_info "$1" "title")"
    date="$(get_post_info "$1" "date")"

    printf "<a class=index href=posts/${file%.md}.html><p class=index><small><em>$date</em></small> <b>$title</b></p></a>\n"
}

print_header "blog/index.css"

printf "<h1>Index</h1>\n"

ENTRIES="$(sort_entries "date" $@)"

while read -r entry ; do
    generate_link "$entry"
    shift
done <<< "$ENTRIES"

print_footer
