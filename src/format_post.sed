# Delete all metadata information
/\[.*=.*\]/d;

# Subsection title
s/==\(.*\)==/<h3>\1<\/h3>/g;

# Section title
s/=\(.*\)=/<h2>\1<\/h2>/g;

# Bold
s/--\(.*\)--/<b>\1<\/b>/g;

# Italic
s/\*\(.*\)\*/<em>\1<\/em>/g;

# Image
# ... with description
s/{\(.*\)|\(.*\)}/<img src=\1><span class=imgdescr>-- \2 --<\/span>/g;
# ... without description
s/{\(.*\)}/<img src=\1>/g;
