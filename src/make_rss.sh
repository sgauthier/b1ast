#!/bin/bash

source "src/utils.sh"

printf '<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">

<channel>
<title>%s</title>
<link>%s</link>
<description>%s</description>\n' "$BLOG_TITLE" "$BLOG_URL" "$BLOG_DESCR"

while [ -n "$1" ] ; do
    html_file="$(basename "$1")"
    html_file="${html_file%.md}.html"
    title="$(get_post_info "$1" "title")"
    date="$(get_post_info "$1" "date")"
    link="$BLOG_URL/posts/$html_file"
    desc="<![CDATA[$(format_post "$1")]]>"
    printf '<item>
<title>%s</title>
<link>%s</link>
<description>%s</description>
<pubDate>%s</pubDate>
</item>\n' "$title" "$link" "$desc" "$date"
    shift
done

printf '</channel>

</rss>'
