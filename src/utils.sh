#!/bin/bash

function get_post_info() {
    echo "$(grep -o "\[$2=.*\]" "$1" | sed -e "s/\[.*=\(.*\)\]/\1/g")"
}

function print_header() {
    CSS="$ROOT_URL/$1"
    TITLE="$2"
    INDEX_URL="$ROOT_URL/blog/index.html"
    cat "html/header.html" | sed -e "s:{style}:$CSS:g;s:{title}:$TITLE:g;s:{index}:$INDEX_URL:g"
}

function print_footer() {
    RSS_URL="$ROOT_URL/blog/rss.xml"
    cat "html/footer.html" | sed -e "s:{RSS}:$RSS_URL:g"
}

function format_post() {
    post="$(basename $1)"
    title="$(get_post_info "$1" "title")"
    date="$(get_post_info "$1" "date")"

    printf "<p><h1>$title</h1></p>\n"
    printf "<p><small><em>Published on $date</em></small></p>\n"

    step1="$(sed -f "src/format_post.sed" "$1")"

    TX=""

    while read -r LINE ; do
        if [[ "$LINE" == "" ]] || [[ "$LINE" =~ "^<.*>$" ]] ; then
            if [ -n "$TX" ] ; then
                printf "\n</p>\n"
                TX=""
            fi
        else
            if [ -z "$TX" ] ; then
                printf "<p>\n"
                TX="yes"
            fi
        fi
        printf "$LINE\n"
    done <<< "$step1"

    if [ -n "$TX" ] ; then
        printf "</p>\n"
    fi
}

function sort_entries() {
    INFO="$1"
    DATA=""
    shift

    while [ -n "$1" ] ; do
        DATA="$(get_post_info "$1" "$INFO") $1"$'\n'"$DATA"
        shift
    done

    #printf "$DATA" > /dev/tty
    SORTED="$(sort -r <<< $DATA)"

    while read -r line ; do
        printf "${line##* }\n"
    done <<< "$SORTED"
}
