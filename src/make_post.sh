#!/bin/bash

source "src/utils.sh"

title="$(get_post_info "$1" "title")"

print_header "blog/styles.css" "$title"

format_post "$1"

print_footer
